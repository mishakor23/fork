#!/bin/bash

if [ ! -d "/vagrant/puppet/modules/nodejs" ]; then
  puppet module install willdurand-nodejs --modulepath=/vagrant/puppet/modules
fi
